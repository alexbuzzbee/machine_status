#!/usr/bin/env python3

# SPDX-License-Identifier: AGPL-3.0-or-later
# Machine status Python CGI script nicknamed "machine_status".
# Copyright (C) 2021 Alex Martin

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""A CGI script that generates a status page.

Outputs a :mailheader:`Content-Type` header, two newlines, then an HTML5 XML document. The output document contains a series of ``<section>`` elements, each one collapsible using a ``<details>`` element nested inside it. These sections contain information about the current status of the machine, such as processed output of :command:`uptime` and :command:`uname`, data from :file:`/etc/os-release`, network status information, and a list of services. This data is marked up for machine readability; every interesting piece of information is assigned an HTML ID and the ``<data>`` and ``<time>`` elements are used to provide more readily-parsed, stable forms of that information.

All input is ignored, whether the standard input stream, environment variables, or command-line arguments.

The script reads a file next to it called :file:`status.conf` in the :py:mod:`configparser` format (effectively INI).
"""

import sys
import socket
import subprocess
import re
from pathlib import Path
import configparser

# If people can access this script via the Web or otherwise remotely,
# THIS URL MUST POINT AT SOMEWHERE A USER CAN GET _YOUR_ VERSION OF THIS CODE (CSS included)
# OR YOU VIOLATE THE AGPL AND COMMIT COPYRIGHT INFRINGEMENT!
# If you have not modified the code, you can leave this as is.
SOURCE_URL = "https://codeberg.org/alexbuzzbee/machine_status"

UPTIME_RE = re.compile(".*(\d{2}:\d{2}:\d{2}) *up *(\d*) *days?, *(\d{1,2}):(\d{2}), *(\d*) *users?, *load average: *(\d*\.\d*), *(\d*\.\d*), *(\d*\.\d*).*|.*(\d{2}:\d{2}:\d{2}) *up *(\d{1,2}):(\d{2}), *(\d*) *users?, *load average: *(\d*\.\d*), *(\d*\.\d*), *(\d*\.\d*).*|.*(\d{2}:\d{2}:\d{2}) *up *(\d{1,2}) min, *(\d*) *users?, *load average: *(\d*\.\d*), *(\d*\.\d*), *(\d*\.\d*).*|.*(\d{2}:\d{2}:\d{2}) *up *(\d*) *days?, *(\d{1,2}) min, *(\d*) *users?, *load average: *(\d*\.\d*), *(\d*\.\d*), *(\d*\.\d*).*|")
VAR_RE = re.compile("^([^\s=]+)=(?:[\s\"']*)(.+?)(?:[\s\"']*)$")
CONF_SERVICE_RE = re.compile("svc-(.*)")
CONF_NETWORK_RE = re.compile("net-(.*)-(.*)")

config = configparser.ConfigParser()
config.read(Path(sys.argv[0]).resolve().parent / "status.conf")

def section(eid, title, content):
    """Generate a collapsible ``<section>`` card.

    :param eid: The value for the element's ``id`` attribute.
    :param title: The text for the heading of the section.
    :param content: HTML representing the section's content.
    :return: HTML representing the entire section.
    """
    return f'''<section id="{eid}">
            <details open="open">
                <summary><h2>{title}</h2></summary>
                {content}
            </details>
        </section>'''

def fields(eid, title, data):
    """Generate a section containing key-value pairs.

    :param eid: The value for the element's ``id`` attribute.
    :param title: The text for the heading of the section.
    :param data: A dictionary of data for the section; keys are used as labels and values as content.
    :return: HTML representing the entire section.
    """
    rows = ""
    for key, value in data.items():
        rows += f'\n                    <dt>{key}:</dt><dd>{value}</dd>'
    return section(eid, title, f'''<dl>{rows}
                </dl>''')

def table(eid, title, data):
    """Generate a section containing a data table.

    :param eid: The value for the element's ``id`` attribute.
    :param title: The text for the heading of the section.
    :param data: A list of dictionaries representing the rows of the table; keys are used as headers and values as cell content. All rows must have the same columns in the same order. Rows may optionsally have a key ``eid``, which will be used as the element ID for the ``<tr>`` element.
    :return: HTML representing the entire section.
    """
    # Generate header row from first record's labels (keys).
    head = "<thead><tr>"
    for label in data[0]:
        if label != "eid":
            head += f'<th>{label}</th>'
    head += "</tr></thead>"

    # Generate each row from values.
    body = "<tbody>"
    for record in data:
        if record["eid"] is not None:
            row = f'\n                        <tr id="{record["eid"]}">'
        else:
            row = '\n                        <tr>'

        for name, value in record.items():
            if name != "eid":
                row += f'<td>{value}</td>'
        row += '</tr>'
        body += row
    body += '\n                    </tbody>'

    return section(eid, title, f'''<table>
                    {head}
                    {body}
                </table>''')

def data(eid, text, value=None, code=False):
    """Generate a ``<data>`` element.

    :param eid: The value for the element's ``id`` attribute.
    :param text: The text content of the element.
    :param value: If given, used as the `value` attribute. Defaults to the value of ``text``.
    :param code: If ``True``, the ``<data>`` element is wrapped in a ``<code>`` element.
    :return: HTML representing the element.
    """
    if value is None:
        value = text
    if code:
        return f'<code><data id="{eid}" value="{value}">{text}</data></code>'
    else:
        return f'<data id="{eid}" value="{value}">{text}</data>'

def parse_env_file(name):
    """Parse a file consisting of ``key=value`` lines with possible double-quotes wrapping the value; this syntax is relatively common on Unix as shell scripts can import it directly into the environment.

    :param name: The name of the file to parse.
    :return: A dictionary containing the values, or an empty dictionary if an error occured.
    """
    try:
        result = {}
        with open(name, "r") as f:
            for line in f:
                var = VAR_RE.match(line)
                if var is not None:
                    result[var.group(1)] = var.group(2)
        return result
    except Exception as err: # Log errors and return no data.
        print(f"Failed to parse env file {name}: {err}", file=sys.stderr)
        return {}

hostname = socket.getfqdn()
def identity():
    """Generate the Identity section, containing the hostname, information from :command:`uname`, and information from :file:`/etc/os-release`.

    :return: HTML representing the entire section.
    """
    os_release = parse_env_file("/etc/os-release")
    name = os_release.get("NAME") or "Unknown"
    ugly_name = os_release.get("ID") or "unknown"
    version = os_release.get("VERSION_ID") or "?"
    codename = os_release.get("VERSION_CODENAME")

    try:
        if ugly_name == "debian":
            with open("/etc/debian_version", "r") as f:
                version = f.read().strip()
        elif ugly_name == "alpine":
            with open("/etc/alpine-release", "r") as f:
                version = f.read().strip()
    except: pass # If getting a more specific version fails, just use the one from os-release

    if codename is not None:
        codenamestr = f' ({data("os-version-codename", codename)})'
    else:
        codenamestr = ""

    kernel = str(subprocess.run(["uname", "-s"], capture_output=True).stdout, encoding="UTF-8").strip()
    kernel_release = str(subprocess.run(["uname", "-r"], capture_output=True).stdout, encoding="UTF-8").strip()
    machine = str(subprocess.run(["uname", "-m"], capture_output=True).stdout, encoding="UTF-8").strip()

    return fields("identity", "Identity", {
        "Hostname": data("hostname-fqdn", hostname, code=True),
        "Kernel": data("uname-kernel-name", kernel),
        "Kernel release": data("uname-kernel-release", kernel_release, code=True),
        "Machine type": data("uname-machine", machine, code=True),
        "Operating system": data("os-name", name, value=ugly_name),
        "OS version": data("os-version", version) + codenamestr
    })

def uptime():
    """Generate the Uptime section, containing information from :command:`uptime` and :command:`uptime --since`.

    :return: HTML representing the entire section.
    """
    # Parse the output of uptime. If a format without days or hours comes back, treat it like zeroes in those positions. If the format isn't recognized at all, show an error.
    uptime_result = str(subprocess.run(["uptime"], capture_output=True).stdout, encoding="UTF-8")
    upsince = str(subprocess.run(["uptime", "--since"], capture_output=True).stdout, encoding="UTF-8").strip()

    uptime_fields = UPTIME_RE.match(uptime_result).groups()
    if uptime_fields[0] is not None:
        [time, days, hours, minutes, users, avg_1, avg_5, avg_15] = uptime_fields[0:8]
    elif uptime_fields[8] is not None:
        [time, hours, minutes, users, avg_1, avg_5, avg_15] = uptime_fields[8:15]
        days = "0"
    elif uptime_fields[15] is not None:
        [time, minutes, users, avg_1, avg_5, avg_15] = uptime_fields[15:21]
        days = "0"
        hours = "0"
    elif uptime_fields[21] is not None:
        [time, days, minutes, users, avg_1, avg_5, avg_15] = uptime_fields[21:29]
        hours = "0"
    else:
        print(f"Failed to parse uptime:\n{uptime_result}", file=sys.stderr)
        return section("uptime-info", "Uptime", f'<p>Error parsing uptime information. Unparsed output:</p><samp id="uptime-output">{uptime_result}</samp><p>Up since <time id="uptime-since">{upsince}</time>.</p>')

    return fields("uptime-info", "Uptime", {
        "Current local time": f'<time id="time-current">{time}</time>',
        "Up for": f'<time id="uptime" datetime="{days}d {hours}h {minutes}m">{days} days, {hours} hours, and {minutes.lstrip("0")} minutes</time>',
        "Up since": f'<time id="uptime-since">{upsince}</time>',
        "Current users": data("users-current", users),
        "Load averages": f'''
                        <table id="load-averages">
                            <thead><tr><th>Minutes</th><th>Load average</th></tr></thead>
                            <tbody>
                                <tr><td>1</td><td>{data("load-avg-1", avg_1)}</td></tr>
                                <tr><td>5</td><td>{data("load-avg-5", avg_5)}</td></tr>
                                <tr><td>15</td><td>{data("load-avg-15", avg_15)}</td></tr>
                            </tbody>
                        </table>
                        <a href="https://en.wikipedia.org/wiki/Load_(computing)#Unix-style_load_calculation">Load averages are complicated.</a>
                    '''
    })

def netinfo(af, pretty_name, conn_type, conn_subtype, conn_dynamic, addr):
    """Generate a section describing the network configuration for a particular address family.

    This section is going to be reorganized to be interface-based instead of address family-based, clean up the type/subtype structure, etc.

    :param af: The address family in question, e.g. ``inet`` or ``inet6``.
    :param pretty_name: A friendly name for the address family, e.g. "IPv4" or "IPv6".
    :param conn_type: The high-level type of the connection, e.g. "Tunnel" or "Direct".
    :param conn_subtype: The sub-type of the connection, such as the name of the tunneling protocol (e.g. "WireGuard", "OpenVPN", "IPSec") or the kind of direct connection (e.g. "NAT", "Private", or "Public").
    :param conn_dynamic: Indicates whether the connection is dynamic (its IP address might change from time to time).
    :param addr: The address for this address family.
    :return: HTML representing the entire section.
    """
    if conn_dynamic:
        dyntext = "Yes"
    else:
        dyntext = "No"

    return fields(f"net-{af}", pretty_name, {
            f"{pretty_name} type": data(f"net-{af}-type", conn_type),
            f"{pretty_name} subtype": data(f"net-{af}-subtype", conn_subtype),
            f"{pretty_name} address is dynamic": data(f"net-{af}-dynamic", dyntext, value=str(conn_dynamic).lower()),
            f"{pretty_name} address": data(f"net-{af}-addr", addr, code=True)
    })

AF_PRETTY_NAMES = {
    "inet6": "IPv6",
    "inet": "IPv4"
}
TYPE_PRETTY_NAMES = {
    "direct": "Direct",
    "tunnel": "Tunnel",
    "nat": "NAT"
}
TUNNEL_PRETTY_NAMES = {
    "": "",
    "wg": "WireGuard",
    "ike2": "IPSec/IKEv2",
    "openvpn": "OpenVPN",
    "ssh": "SSH",
    "l2tp": "IPSec/L2TP",
    "pptp": "PPTP",
    "sstp": "SSTP"
}
def network(interface, af, conn_type, tunnel, peer, dyn, addr):
    """Generate a table row describing a network configuration (interface + address family pair) used by this machine.

    :param interface: The interface name, e.g. ``eth0`` or ``wlp2s1``.
    :param af: The address family name, e.g. ``inet`` or ``inet6``.
    :param conn_type: The type of the connection. One of: ``direct`` for a straightforward Internet link, ``tunnel`` for a tunnel over a different network link to another machine, or ``nat`` if affected by Network Address Translation.
    :param tunnel: The type of the tunnel, or ``None`` if type other than ``tunnel``. One of: ``wg`` for WireGuard, ``ike2`` for IPSec/IKEv2, ``openvpn`` for OpenVPN, ``ssh`` for SSH, ``l2tp`` for IPSec/L2TP, ``pptp`` for PPTP, or ``SSTP`` for SSTP.
    :param peer: The hostname or address of the other end of the tunnel, or ``None`` if type other than ``tunnel``.
    :param dyn: Whether the address can change from time to time.
    :param addr: The current address.
    :return: A dictionary representing a row for the networks table.
    """
    pretty_af = AF_PRETTY_NAMES[af]
    pretty_type = TYPE_PRETTY_NAMES[conn_type]
    pretty_tunnel = TUNNEL_PRETTY_NAMES[tunnel or ""]
    name = f"net-{interface}-{af}"
    if dyn:
        pretty_dyn = "Yes"
    else:
        pretty_dyn = "No"

    return {
        "eid": name,
        "Interface name": data(f"{name}-interface", interface, code=True),
        "Address family": data(f"{name}-af", pretty_af, value=af),
        "Type": data(f"{name}-type", pretty_type, value=conn_type),
        "Tunnel kind": data(f"{name}-tunnel", pretty_tunnel, value=(tunnel or "")),
        "Tunnel peer": data(f"{name}-tunnel-peer", peer or "", code=True),
        "Address is dynamic?": data(f"{name}-dynamic", pretty_dyn, value=dyn),
        "Address": data(f"{name}-addr", addr, code=True)
    }

def networks():
    """Generate the Network section, containing a table of network configurations used by this machine.

    :return: HTML representing the entire section.
    """
    network_info = []
    for name, section in config.items():
        match = CONF_NETWORK_RE.match(name)
        if match is not None:
            interface = match.group(1)
            af = match.group(2)
            network_info.append(network(interface, af, section["type"], section.get("tunnel"), section.get("tunnel-peer"), section["dynamic"] == "true", section["addr"]))

    return table("net", "Network", network_info)

def systemctl_active(service):
    """Get a small amount of information about a daemon's status, according to :command:`systemctl status`.

    :param service: The name of the daemon, without :program:`systemd`'s ``.service`` suffix.
    :return: The text of the "Active:" line from the output of :command:`systemctl status`.
    """
    return str(subprocess.run(f"systemctl status {service}.service | sed -nEe 's/[[:space:]]+Active: (.*)/\\1/p'", shell=True, capture_output=True).stdout, encoding="UTF-8")

def service(eid, nickname, url, daemon, manual_status):
    """Generate a table row describing a service that this machine provides. A status check is automatically performed, and it is reported if the service is down.

    :param nickname: A friendly name for the service (e.g. "files", "mail", "[hostname] status"). May contain HTML.
    :param url: A URL describing how the service is exposed on the network (e.g. protocol, port, and domain name).
    :param daemon: The name of the daemon that runs this service.
    :param manual_status: A manual override to the automated service status check.
    :return: A dictionary representing a row for the services table.
    """
    systemctl_result = systemctl_active(daemon)
    if manual_status == "available" and "running" in systemctl_result:
        available = True
        down_reason = data(f"{eid}-reason", "")
    elif "running" not in systemctl_result:
        available = False
        down_reason = " (" + data(f"{eid}-reason", "down") + ")"
    else:
        available = False
        down_reason = " (" + data(f"{eid}-reason", manual_status) + ")"

    if available:
        avstr = data(f"{eid}-available", "Yes", value="true")
    else:
        avstr = data(f"{eid}-available", "No", value="false")

    return {
        "eid": eid,
        "Nickname": data(f"{eid}-nickname", nickname),
        "URL": data(f"{eid}-url", url, code=True),
        "Available?": f"{avstr}{down_reason}",
        "Daemon": data(f"{eid}-daemon", daemon, code=True),
        "Daemon status": f'<samp id="{eid}-systemctl-status-running">{systemctl_result}</samp>'
    }

def services():
    """Generate the Services section, containing a table of services provided by this machine. Information about services is read from the configuration file.

    :return: HTML representing the entire section.
    """
    service_info = []
    for name, section in config.items():
        match = CONF_SERVICE_RE.match(name)
        if match is not None:
            service_info.append(service(name, section["nickname"], section["url"], section["daemon"], section["status"]))

    return table("services", "Services", service_info)

# If people can access this script via the Web or otherwise remotely,
# THIS FUNCTION AND ITS INVOCATION OR SOMETHING EQUIVALENT CANNOT BE OMITTED
# WITHOUT VIOLATING THE AGPL AND COMMITTING COPYRIGHT INFRINGEMENT!
def agpl():
    """Generate a section informing the user of their rights under the GNU AGPL and, in particular, giving a link to the source code. Removing this section is usually illegal; consult the AGPL itself for more information.

    :return: HTML representing the entire section.
    """
    return section("license", "Licensing information", f'<p>The program used to generate this status page is licensed under the <a rel="license code-license" href="https://www.gnu.org/licenses/agpl-3.0.en.html">GNU Affero General Public License version 3</a> (or, at your option, a later version). This gives you the right to <a rel="code-repository" href="{SOURCE_URL}">a copy of the source code for this version of that program</a>.</p>')

print(f'''Content-Type: application/xhtml+xml; charset=UTF-8

<?xml version="1.0" encoding="UTF-8"?>
<!-- SPDX-License-Identifier: AGPL-3.0-or-later -->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">
    <head>
        <title>{hostname} status</title>
        <link rel="stylesheet" href="status.css" />
    </head>
    <body>
        <h1><code>{hostname.split('.')[0]}</code> status</h1>
        {identity()}
        {uptime()}
        {networks()}
        {services()}
        {agpl()}
    </body>
</html>
''')

