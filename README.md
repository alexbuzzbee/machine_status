# `machine_status`

A status page CGI script in Python. Only tested with Apache. If you want to use this, you'll need a configuration file; you can start from `status.conf.example`. Edit it to match your configuration, rename it `status.conf`, and put it in the same directory as the script. You might want to configure your server to stop people from downloading the configuration file.

Shows a series of collapsible "cards" with various information. The internal structure is reasonably modular, so it should be relatively easy to modify it to handle a given use case.

The following information is currently shown:

* Identity information: The full DNS domain name, kernel name and release, machine type, and operating system and release.
* Uptime information: The current local time, number of days, hours, and minutes up, number of current user sessions, and load averages.
* Network information: The interface name, address family, connection type, (optionally) tunnel type and peer, address, and whether dynamic for each network configuration given in the configuration file.
* Service information: A nickname, URL, availability status, daemon name, and status printout for each service given in the configuration file.
* The legally-required notice for the GNU Affero General Public License.

The page is valid XHTML, and all of this information is marked up with HTML IDs and the `<data>` and `<time>` elements where appropriate, so it should be relatively easy to extract it programmatically. A reasonable amount of effort will be put into keeping IDs stable as layout etc. changes, but sometimes a functional change necessitates disruption.
